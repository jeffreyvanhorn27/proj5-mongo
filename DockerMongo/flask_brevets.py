"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os

import logging

###
# Globals
###
app = flask.Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevetdb

CONFIG = config.configuration()
#app.secret_key = CONFIG.SECRET_KEY
app.secret_key = os.urandom(24)
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")

    #test code to make sure the database is working
    _items = db.brevetdb.find()
    items = [item for item in _items]

    for item in items:
        delete()
        break
        print(item)

    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

def is_duplicate(km):
    _brevets = db.brevetdb.find()
    brevets = [brevet for brevet in _brevets]

    for brevet in brevets:
        if brevet["km"] == km:
            return True
    return False

#Routing added for project 5

@app.route('/_submit')
def submit():
    print("submit works")
    #need to add the new brevet to the database
    km = request.args.get('km', 1000000, type=float)
    app.logger.debug(km)
    ml = request.args.get('ml', 1000000, type=float)
    app.logger.debug(ml)
    location = request.args.get('location', "", type=str)
    app.logger.debug(location)
    open_time = request.args.get('open', "", type=str)
    app.logger.debug(open_time)
    close_time = request.args.get('close', "", type=str)
    app.logger.debug(close_time)

    #now we need add this to the db
    brevet_doc = {
        'km': km,
        'miles': ml,
        'location': location,
        'open_time': open_time,
        'close_time': close_time
    }

    #check to see if the brevet is a duplicate


    if brevet_doc["km"] != 1000000 and brevet_doc["miles"] != 1000000 and open_time != "Invalid date" and close_time != "Invalid date":
        app.logger.debug("insert")
        #check to see if the brevet is a duplicate
        dup = is_duplicate(brevet_doc["km"])
        result = {"error": False}
        if dup:
            result["error"] = True
        else:
            db.brevetdb.insert_one(brevet_doc)
        
        return flask.jsonify(result=result)
    else:
        app.logger.debug("error")
        result = {"error": True}
        return flask.jsonify(result=result)
        #return "error"

    #return flask.render_template("calc.html"), 200




@app.route("/display")
def display():
    print("display route")
    #need to update the code to get all of the brevets to show up
    _brevets = db.brevetdb.find()
    brevets = [brevet for brevet in _brevets]
    return flask.render_template("display.html", brevets=brevets), 200

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    bl = request.args.get('bl', 999, type=int)
    bt = request.args.get('bt', "", type=str)
    print("bl: {}".format(bl))
    print("bt: {}".format(bt))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    valid = True

    try:
        begin_time = arrow.get(bt)
        print("arrow made: {}".format(begin_time))

        app.logger.debug("bl: {}".format(bl))
        app.logger.debug("bt: {}".format(begin_time.isoformat()))

        open_time = acp_times.open_time(km, bl, begin_time.isoformat())
        close_time = acp_times.close_time(km, bl, begin_time.isoformat())

        app.logger.debug("open: {}".format(open_time))
        app.logger.debug("close: {}".format(close_time))
    except:
        app.logger.debug("check: {}".format("hit"))
        valid = False
        app.logger.debug("valid: {}".format(valid))
        app.logger.debug("check2: {}".format("past"))
        open_time = "Invalid Date"
        close_time = "Invalid Date"
    app.logger.debug("test: {}".format(open_time))
    if open_time == "Invalid Date" or close_time == "Invalid Date":
        valid = False
    
    #fix the open_time
    #get the length of the brevet
    """
    brev_length = 
    open_time = acp_times.open_time(km, )
    """
    result = {"open": open_time, "close": close_time, "valid": valid}
    app.logger.debug("check res: {}".format(result["valid"]))
    return flask.jsonify(result=result)

def delete():
    db.brevetdb.drop()

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.debug = True
    #delete()
    print("Opening for global access on port {}".format(CONFIG.PORT))
    print(acp_times.close_time(0, 400, arrow.get("2017-02-01T09:30")))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
